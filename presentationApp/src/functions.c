#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <epicsStdio.h>
#include <epicsString.h>
#include <iocsh.h>
#include <registryFunction.h>
#include <epicsExport.h>
#include <ellLib.h>

static int count = 0;

epicsShareFunc int present(const char* arg0, const int arg1) {
    printf("Argument: %s\n", arg0);
    printf("Old count: %d, ", count);
    count += arg1;
    printf("new count: %d\n", count);
    return 0;
}

static const iocshArg presentArg0 = {"argument", iocshArgString};
static const iocshArg presentArg1 = {"number", iocshArgInt};
static const iocshArg *const presentArgs[] = {&presentArg0, &presentArg1};
static const iocshFuncDef presentDef = {"present", 2, presentArgs};
static void presentCall(const iocshArgBuf *args)
{
    present(args[0].sval, args[1].ival);
}

static void presentRegister(void)
{
    static int firstTime = 1;
    if (!firstTime)
        return;
    firstTime = 0;
    iocshRegister(&presentDef, presentCall);
}

epicsExportRegistrar(presentRegister);
